﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Proyecto_AdEMP.Models
{
    public class crearemp
    {
        public class emp
        {
            [Key]
            public string DNI
            {
                get;
                set;
            } 
            public string usuario
            {
                get;
                set;
            }
            public string clave
            {
                get;
                set;
            }
            public string claveconfirmada
            {
                get;
                set;
            }
            public string nombre
            {
                get;
                set;
            }
            public string apellido
            {
                get;
                set;
            }
            public string oficio
            {
                get;
                set;
            }
            public string categoria
            {
                get;
                set;
            }
            public string estado
            {
                get;
                set;
            }
            public string horario1
            {
                get;
                set;
            }
            public string activo
            {
                get;
                set;
            }
        }

        public class CmsDbContext : AdEMPEntities
        {
            public DbSet<emp> ObjRegisterUser { get; set; } 
        }
    }
}