create database AdEMP;

use AdEMP;

	/* Creacion tabla de categoria  */
create table categoria(
    id_categoria INT PRIMARY KEY NOT NULL,
	nombre_categoria VARCHAR(30) NOT NULL,
	salario INT DEFAULT NULL,	
	horas_totales INT NOT NULL
	);
	
	/* Creacion tabla de horarios */
create table horarios(
	id_horario int PRIMARY KEY NOT NULL,
	tipo_horario VARCHAR(50),
	lunes VARCHAR(10) DEFAULT NULL,
	martes VARCHAR(10) DEFAULT NULL,
	miercoles VARCHAR(10) DEFAULT NULL,
	jueves VARCHAR(10) DEFAULT NULL,
	viernes VARCHAR(10) DEFAULT NULL
	);
	

	/* Creacion tabla estado del usuario */
create table estado(
		codigo INT PRIMARY KEY NOT NULL,
		estado VARCHAR(50)
		);
	
create table activo(
        codigo INT PRIMARY KEY NOT NULL,
        activo VARCHAR(2)
        );

create table oficio(
        codigo INT PRIMARY KEY NOT NULL,
        oficio VARCHAR(50)
        );		
		
	/* Creacion tabla empleados */
create table empleado(
	DNI VARCHAR (9) NOT NULL PRIMARY KEY,	
	usuario VARCHAR(50) NOT NULL UNIQUE,	
	clave VARCHAR(100) NOT NULL,
	claveconfirmada VARCHAR(100) NOT NULL,
	nombre VARCHAR(50) NOT NULL,	
	apellido VARCHAR(50) NOT NULL,	
	oficio INT NOT NULL,	
	categoria INT NOT NULL,	
	inicio_conex DATETIME DEFAULT NULL, /*Nos indica la conexion del ultimo dia*/
	ult_conex DATETIME DEFAULT NULL, /*Nos indica la conexion de salido del ultimo dia*/	
	estado INT NOT NULL,
	horario int NOT NULL,
	activo INT NOT NULL,
    CONSTRAINT "empleado_fk_categoria" FOREIGN KEY ("categoria") REFERENCES "categoria" ("id_categoria") ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT "empleado_fk_horario_trabajador" FOREIGN KEY ("horario") REFERENCES "horarios" ("id_horario") ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT "empleado_fk_estado" FOREIGN KEY ("estado") REFERENCES "estado" ("codigo") ON DELETE CASCADE ON UPDATE CASCADE,	
	CONSTRAINT "empleado_fk_activo" FOREIGN KEY ("activo") REFERENCES "activo" ("codigo") ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT "empleado_fk_oficio" FOREIGN KEY ("oficio") REFERENCES "oficio" ("codigo") ON DELETE NO ACTION ON UPDATE CASCADE
	);		
	
	/* Creacion tabla Historial de conexiones */	
create table historico_conexion(
		id int IDENTITY(1,1) PRIMARY KEY,
		DNI varchar(9), 
		fecha varchar(50),
		registro varchar(200),
		CONSTRAINT "registroconexion_fk_DNI" FOREIGN KEY (DNI) REFERENCES "empleado" (DNI) ON DELETE CASCADE ON UPDATE CASCADE 
	);	
		
	
	/* Insercion de datos de categoria */
insert into categoria (id_categoria, nombre_categoria, salario, horas_totales) VALUES
	/*Junior*/
	(10, 'Junior A', 500, 20),
	(11, 'Junior B', 750, 30),
	(12, 'Junior C', 875, 35),
	(13, 'Junior D', 1100, 40),
	/*Senior*/	
	(20, 'Senior A', 750, 20),
	(21, 'Senior B', 1000, 30),
	(22, 'Senior C', 1250, 35),
	(23, 'Senior D', 1500, 40),
	(24, 'Senior E', 1950, 40),
	(25, 'Senior F', 2500, 40),
	(26, 'Senior G', 1100, 20),
	(27, 'Senior H', 1600, 30),
	/*Diseñador*/	
	(30, 'Diseñador A', 900, 20),
	(31, 'Diseñador B', 1400, 30),
	/*Operador Hardware*/	
	(40, 'Operador Hardware', 850, 20),
	/*Gestor Base de datos*/	
	(50, 'Gestor Base Datos', 1800, 30),
	/*Administrador*/
	(60, 'Administrador', 4000, 40);
	
	
	/* Insercion de datos de los horarios */	
insert into horarios (id_horario, tipo_horario, lunes, martes, miercoles, jueves, viernes) VALUES
	/*Horario de 20 horas */
	(1,'20 horas 1º', '10-14', '10-14', '10-14', '10-14', '10-14'),
	(2,'20 horas 2º', '8-12', '8-12', '8-12', '8-12', '8-12'),	
	(3,'20 horas 3º', '16-20', '16-20', '16-20', '16-20', '16-20'),		
	(4,'20 horas 4º', '10-14', '16-20', '10-14', '16-20', '10-14'),	
	(5,'20 horas 5º', '16-20', '10-14', '16-20', '16-20', '10-14'),		
	(6,'20 horas 6º', '10-14', '16-20', '16-18', '8-14', '12-14'),	
	/*Horario de 30 horas */
	(7,'30 horas 1º', '10-16', '10-16', '10-16', '10-16', '10-16'),
	(8,'30 horas 2º', '8-14', '8-14', '8-14', '8-14', '8-14'),	
	(9,'30 horas 3º', '14-20', '14-20', '14-20', '14-20', '14-20'),		
	(10,'30 horas 4º', '8-14', '14-20', '8-14', '14-20', '8-14'),	
	(11,'30 horas 5º', '14-20', '10-16', '14-20', '14-20', '10-16'),		
	(12,'30 horas 6º', '10-16', '14-20', '10-18', '8-14', '10-14'),		
	/*Horario de 35 horas */
	(13,'35 horas 1º', '10-17', '10-17', '10-17', '10-17', '10-17'),
	(14,'35 horas 2º', '8-15', '8-15', '8-15', '8-15', '8-15'),	
	(15,'35 horas 3º', '13-20', '13-20', '13-20', '13-20', '13-20'),		
	(16,'35 horas 4º', '8-15', '10-17', '8-15', '10-17', '8-15'),	
	(17,'35 horas 5º', '13-20', '8-15', '10-17', '13-20', '8-15'),		
	(18,'35 horas 6º', '8-16', '10-18', '8-15', '12-20', '10-14'),
	/*Horario de 40 horas */	
	(19,'40 horas 1º', '10-18', '10-18', '10-18', '10-18', '10-18'),
	(20,'40 horas 2º', '8-16', '8-16', '8-16', '8-16', '8-16'),	
	(21,'40 horas 3º', '12-20', '12-20', '12-20', '12-20', '12-20'),		
	(22,'40 horas 4º', '10-18', '12-20', '10-18', '12-20', '10-18'),	
	(23,'40 horas 5º', '12-20', '10-18', '12-20', '12-20', '8-16'),		
	(24,'40 horas 6º', '8-17', '10-19', '9-18', '11-20', '10-14');	
	
	
	/* Insercion de datos de estados del usuario */	
insert into estado (codigo, estado) VALUES
	(0, '-----------------'),
	(1, 'Puesto de trabajo'),
	(2, 'Oficina'),
	(3, 'Reunion'),
	(4, 'Descanso');	
	
	/* Insercion de activo o no activo */	
insert into activo (codigo, activo) VALUES
	(1, 'SI'),
	(2, 'NO');	
	
		/* Insercion de oficio */	
insert into oficio (codigo, oficio) VALUES
	(1, 'Programador'),
	(2, 'Tester'),
	(3, 'Desarrollador'),
	(4, 'Analista'),
	(5, 'Developer'),
	(6, 'Sistemas'),
	(7, 'Web'),
	(8, 'Jefe'),
	(9, 'Director');	
	
	/*usuario de prueba */
insert into empleado(DNI, usuario, clave, claveconfirmada, nombre, apellido, oficio, categoria, estado, horario, activo) VALUES 
	('59129523A', 'alex', '6DxtMmfXTu','B8-4B-2B-06-27-B8-51-9D-9F-E7-76-CC-29-3D-DC-FE', 'Aleajndro', 'Miquel', 1, 60, 3, 1, 2);
	
	
	
-- TRIGGER Que realizara el registro de horas de inicio de sesion y salida
go
CREATE TRIGGER AdEMP_registro  
   ON  empleado  
   AFTER UPDATE 
AS   
BEGIN  
    SET NOCOUNT ON;  
		if update (ult_conex)
			begin
    INSERT INTO historico_conexion   
    (DNI, fecha, registro)  
    SELECT 
		DNI, 
		convert(varchar(50),GETDATE(),103 ), 
		'El empleado '
			+nombre + ' ' 
			+apellido 
			+ ' inicio sesion el dia ' 
			+ convert(varchar(50),inicio_conex,103 ) 
			+ ' a las ' 
			+ Replace(convert(varchar(50),inicio_conex,108), '',':')
			+ ' y se desconecto a las ' 
			+ Replace(convert(varchar(50),ult_conex,108), '' , ':')
			+ ', ha trabajado un total de '
			+ convert(varchar(50),DATEDIFF(HOUR, inicio_conex,ult_conex))
			+ ' horas'  
    FROM INSERTED  
--Los valores que se insertaran, seran los que esten almacenados en la tabla virtual Inserted  
END  
END
GO 


-- Trigger que borra los registros nulos cuando se modifica un usuario
GO
CREATE TRIGGER AdEMP_BorraNull  
   ON  historico_conexion  
   AFTER Insert 
		AS   
		BEGIN  
    SET NOCOUNT ON;  
		if exists  (Select registro from historico_conexion)
			begin
    delete from historico_conexion where registro is NULL  
END  
END
GO
 	