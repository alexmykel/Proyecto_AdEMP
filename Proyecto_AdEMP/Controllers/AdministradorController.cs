﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_AdEMP;
using Proyecto_AdEMP.Models;
using Proyecto_AdEMP.Controllers;
using static Proyecto_AdEMP.Models.crearemp;

namespace Proyecto_AdEMP.Controllers
{
    public class AdministradorController : Controller
    {
        private AdEMPEntities db = new AdEMPEntities();


        //Inicio Sesion Usuario Administrador
        public ActionResult Index(string id)
        {
            if (Session["DNI"] != null)
            {
                id = Session["DNI"].ToString();
                empleado empleado = db.empleado.Find(id);
                ViewBag.estado = new SelectList(db.estado, "codigo", "estado1", empleado.estado);
                ViewBag.activo = new SelectList(db.activo, "codigo", "activo1", empleado.activo);

                return View(empleado);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "DNI,usuario,clave,nombre,apellido,oficio,categoria,inicio_conex,ult_conex,foto,estado,horario,activo")] empleado empleado)
        {
            ViewBag.estado = new SelectList(db.estado, "codigo", "estado1", empleado.estado);
            
            if (ModelState.IsValid)
            {
                ViewBag.estado = new SelectList(db.estado, "codigo", "estado1");
                db.SaveChanges();
                return RedirectToAction("empleado", "index");
            }

            ViewBag.activo = new SelectList(db.activo, "codigo", "activo1", empleado.activo);
            ViewBag.categoria = new SelectList(db.categoria, "id_categoria", "nombre_categoria", empleado.categoria);

            ViewBag.horario = new SelectList(db.horarios, "id_horario", "id_horario", empleado.horario);
            return View(empleado);
        }



        // VISTA Empleados de trabajo
        public ActionResult Empleados(String nombre)
        {
            var empleado = db.empleado.Include(e => e.activo1).Include(e => e.categoria1).Include(e => e.estado1).Include(e => e.horarios).Include(e => e.oficio1).Include(e=>e.historico_conexion);
            var historico_conexion = db.historico_conexion.Include(h => h.empleado);
         //Buscador de usuario por nombre para compañeros
            var nombrebusqueda = from usuario in db.empleado select usuario;
            if (!String.IsNullOrEmpty(nombre))
            {
                nombrebusqueda = nombrebusqueda.Where(model => model.nombre.Contains(nombre));
            }
            return View(nombrebusqueda);
        }

        // Vista horario trabajador
        public ActionResult horario(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            empleado empleado = db.empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        public ActionResult Modificar()
        {
            return View();
        }


        //VISTA DE CREACION DE USUARIO
        public ActionResult CrearUsuario()
        {
            ViewBag.activo = new SelectList(db.activo, "codigo", "activo1");
            ViewBag.categoria = new SelectList(db.categoria, "id_categoria", "nombre_categoria");
            ViewBag.estado = new SelectList(db.estado, "codigo", "estado1");
            ViewBag.horario = new SelectList(db.horarios, "id_horario", "tipo_horario");
            ViewBag.oficio = new SelectList(db.oficio, "codigo", "oficio1");
            return View();
        }


        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CrearUsuario(empleado NuevoUsuario)
        {
            try
            {
                ViewBag.activo = new SelectList(db.activo, "codigo", "activo1");
                ViewBag.categoria = new SelectList(db.categoria, "id_categoria", "nombre_categoria");
                ViewBag.estado = new SelectList(db.estado, "codigo", "estado1");
                ViewBag.horario = new SelectList(db.horarios, "id_horario", "tipo_horario");
                ViewBag.oficio = new SelectList(db.oficio, "codigo", "oficio1");

                using (var db = new AdEMPEntities())
                {
                    //CREAREMOS EL NUEVO USUARIO Y CODIFICAREMOS SU CONTRASEÑA
                    var chkUser = (from s in db.empleado where s.usuario == NuevoUsuario.usuario || s.DNI == NuevoUsuario.DNI select s).FirstOrDefault();
                    if (chkUser == null)
                    {
                        //ESTABLECEREMOS QUE CLAVE SERA = QUE CLAVECONFIRMADA PARA QUE TENGAN EL MISMO VALOR 
                        NuevoUsuario.claveconfirmada = NuevoUsuario.clave;
                        var keyNew = ClaveHash.GeneratePassword(10);
                        var password = ClaveHash.EncodePassword(NuevoUsuario.claveconfirmada, keyNew);
                        NuevoUsuario.claveconfirmada = password;
                        NuevoUsuario.clave = keyNew;
                        db.empleado.Add(NuevoUsuario);
                        db.SaveChanges();
                        ModelState.Clear();
                        return RedirectToAction("Modificar", "Administrador");
                    }
                    ViewBag.ErrorMessage = "El usuario ya existe";
                    return View();
                }
            }
            catch (Exception error)
            {
                ViewBag.ErrorMessage = "Han ocurrido algunos problemas" + error;
                return View();
            }
        }


        //EDITAR EMPLEADOS
        public ActionResult EditarEmpleado(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            empleado empleado = db.empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            ViewBag.activo = new SelectList(db.activo, "codigo", "activo1", empleado.activo);
            ViewBag.categoria = new SelectList(db.categoria, "id_categoria", "nombre_categoria", empleado.categoria);
            ViewBag.estado = new SelectList(db.estado, "codigo", "estado1", empleado.estado);
            ViewBag.horario = new SelectList(db.horarios, "id_horario", "tipo_horario", empleado.horario);
            ViewBag.oficio = new SelectList(db.oficio, "codigo", "oficio1", empleado.oficio);
            return View(empleado);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarEmpleado(empleado ModificarUsuario)
        {
            ViewBag.activo = new SelectList(db.activo, "codigo", "activo1");
            ViewBag.categoria = new SelectList(db.categoria, "id_categoria1", "nombre_categoria");
            ViewBag.estado = new SelectList(db.estado, "codigo", "estado1");
            ViewBag.horario = new SelectList(db.horarios, "id_horario", "tipo_horario");
            ViewBag.oficio = new SelectList(db.oficio, "codigo", "oficio1");
            if (ModelState.IsValid)
            {
                using (var db = new AdEMPEntities())
                {
                    //CODIFICAREMOS LAS COLUMNAS CLAVE Y CLAVE CONFIRMADA PARA GENERAR LA CLAVE HASH SI EL USUARIO CAMBIA LA CONTRASEÑA
                    ModificarUsuario.claveconfirmada = ModificarUsuario.clave;
                    var keyNew = ClaveHash.GeneratePassword(10);
                    var password = ClaveHash.EncodePassword(ModificarUsuario.claveconfirmada, keyNew);
                    ModificarUsuario.claveconfirmada = password;
                    ModificarUsuario.clave = keyNew;
                    db.Entry(ModificarUsuario).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Modificar", "Administrador");
                }
            }

            return View(ModificarUsuario);
        }


        //ACCION DE BORRAR AL EMPLEADO
        public ActionResult BorrarEmpleado(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            empleado empleado = db.empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }

        // POST: BORRADO DEL EMPLEADO
        [HttpPost, ActionName("BorrarEmpleado")]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmarBorrar(string id)
        {
            empleado empleado = db.empleado.Find(id);
            db.empleado.Remove(empleado);
            db.SaveChanges();
            return RedirectToAction("Empleados");
        }


        //REGISTRO DE HORAS DE CONEXION DE LOS EMPLEADOS
        public ActionResult RegistroHorario(string DNI)
        {
            //BUSCADOR POR DNI
            var nombrebusqueda = from id in db.historico_conexion select id;
            if (!String.IsNullOrEmpty(DNI))
            {
                nombrebusqueda = nombrebusqueda.Where(model => model.DNI.Contains(DNI));
                return View(nombrebusqueda);
            }

            return View(db.historico_conexion.ToList());
        }


        //HORARIOS DE LOS TRABAJADORES
        public ActionResult Horarios(horarios horario)
        {
            var empleado = db.empleado.Include(e => e.activo1).Include(e => e.categoria1).Include(e => e.estado1).Include(e => e.horarios);
            ViewBag.horarios = new SelectList(db.horarios, "tipo_horario", "lunes", "martes", "miercoles", "jueves", "viernes");
            return View(db.horarios.ToList());
        }


        //CATEGORIA, CONTENIDO DE SALARIO Y CATEGORIA (JUNIOR,SENIOR,ANALISTA....)
        public ActionResult Categoria()
        {
            ViewBag.categoria = new SelectList(db.categoria, "id_categoria", "nombre_categoria");
            return View(db.categoria.ToList());
        }


        //EDITAR SALARIO DE CADA CATEGORIA
        public ActionResult EditarCategoria(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            categoria categoria = db.categoria.Find(id);
            if (categoria == null)
            {
                return HttpNotFound();
            }
            return View(categoria);
        }

        // Guardar y editar salario de categoria
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarCategoria([Bind(Include = "id_categoria, nombre_categoria, salario, horas_totales")] categoria categoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Categoria", "Administrador");
            }
            return View(categoria);
        }

        //VISTA SECCION ESTADISTICAS
        public ActionResult Estadisticas()
        {
            return View();
        }

        //VISTA INFORMACION AdEMP
        public ActionResult AdEMP()
        {
            return View();
        }

        //VISTA CONTACTO
        public ActionResult Contacto()
        {
            return View();
        }


        //ACCION DESCONECTAR
        public ActionResult Desconectar(string id)
        {
            empleado empleado = db.empleado.Find(id);
            return View(empleado);
        }

        [HttpPost]
        public ActionResult Desconectar(empleado objUser)
        {
            if (ModelState.IsValid)
            {
                using (AdEMPEntities db = new AdEMPEntities())
                {
                    //DESCONEXION AL PASAR PARAMETRO NOMBRE DEL USUARIO
                    var obj = db.empleado.Where(session => session.usuario.Equals(objUser.usuario)).FirstOrDefault();
                    try
                    {
                        if (objUser.usuario == obj.usuario.ToString())
                        {
                            //Al coincidir el usuario registrara el dato obtenido de la hora en la que se ha desconectado 
                                                             //y cambio de activo a 2(false)
                            obj.ult_conex = DateTime.Now;
                            obj.activo = 2;
                            db.SaveChanges();
                            Session.Abandon();
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            //SI EL VALOR ES NULO NOS DEVUELVE A LA VISTA DE DESCONECTAR
                            return RedirectToAction("Desconectar", "Administrador");
                        }
                    }
                    catch
                    {
                        return RedirectToAction("Desconectar", "Administrador");
                    }
                }
            }
            return View(objUser);

        }





        






     






        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }




    }
}
