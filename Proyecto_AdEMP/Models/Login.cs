﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_AdEMP.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Introduzca su usuario")]
        public string usuario { get; set; }
        [Required(ErrorMessage = "Introduzca la contraseña")]
        public string clave { get; set; }
    }


}