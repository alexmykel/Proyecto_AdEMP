﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="estadisticaEmpleado.aspx.cs" Inherits="Proyecto_AdEMP.estadisticaEmpleado" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource1" Height="419px" Width="554px">
            <series>
                <asp:Series ChartType="StackedColumn" LabelForeColor="Transparent" Legend="Legend1" Name="Series1" XValueMember="activo" YValueMembers="codigo">
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
            <Legends>
                <asp:Legend Name="Legend1">
                </asp:Legend>
            </Legends>
        </asp:Chart>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AdEMPConnectionString %>" SelectCommand="SELECT * FROM [activo]"></asp:SqlDataSource>
    </form>
</body>
</html>
