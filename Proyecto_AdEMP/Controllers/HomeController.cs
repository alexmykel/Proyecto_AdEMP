﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_AdEMP;
using Proyecto_AdEMP.Models;
using Proyecto_AdEMP.Controllers;
using System.Security.Cryptography;


namespace Proyecto_AdEMP.Controllers
{
    public class HomeController : Controller
    {
        private AdEMPEntities db = new AdEMPEntities();

        // GET: Login
        public ActionResult Index()
        {
                return View();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(empleado objUser, string usuario, string clave)
        {
            if (ModelState.IsValid)
            {
                using (AdEMPEntities db = new AdEMPEntities())
                {
                    var obj = db.empleado.Where(session => session.usuario.Equals(objUser.usuario)).FirstOrDefault();

                    if (obj != null)
                    {
                        var hashCode = obj.clave;
                        //Proceso de llamada a la clase Hash para verificar la clave Hash    
                        var encodingPasswordString = ClaveHash.EncodePassword(clave, hashCode);
                        //Verificar clave y usuario para iniciar sesion
                        var query = (from s in db.empleado where (s.usuario == usuario) && s.claveconfirmada.Equals(encodingPasswordString) select s).FirstOrDefault();
                        if (query != null)
                        {
                            //Inicio de sesion guardando cuando hemos iniciado la sesion y cambiando nuestro estado a activo SI (true)
                            obj.inicio_conex = DateTime.Now;
                            obj.activo = 1;

                            db.SaveChanges();
                            Session["DNI"] = obj.DNI.ToString();
                            Session["usuario"] = obj.usuario.ToString();
                            Session["nombre"] = obj.nombre.ToString();
                            Session["apellido"] = obj.apellido.ToString();
                            Session["activo"] = "Si";
                            Session["inicio_conex"] = obj.inicio_conex.ToString();
                            Session["ult_conex"] = obj.ult_conex.ToString();

                            //Redireccionamiento por rol de categoria
                            if (obj.categoria == 60 || obj.categoria == 50)
                            {
                                return RedirectToAction("Index", "Administrador");
                            }
                            else
                            {
                                return RedirectToAction("Index", "Empleados");
                            }
                        }
                    }
                }
            }
            return View(objUser);
        }

    }
}