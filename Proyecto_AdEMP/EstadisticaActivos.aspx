﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EstadisticaActivos.aspx.cs" Inherits="Proyecto_AdEMP.EstadisticaActivos" %>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable(<%=obtenerDatos()%>);

            var options = {
                title: 'Porcentaje dinamico de usuarios activos'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }
    </script>

    <script  type="text/javascript">
        function tiempo() {
            momentoActual = new Date()
            hora = momentoActual.getHours()
            minuto = momentoActual.getMinutes()
            segundo = momentoActual.getSeconds()

            str_segundo = new String(segundo)
            if (str_segundo.length == 1)
                segundo = "0" + segundo

            str_minuto = new String(minuto)
            if (str_minuto.length == 1)
                minuto = "0" + minuto

            str_hora = new String(hora)
            if (str_hora.length == 1)
                hora = "0" + hora

            horaImprimible = hora + " : " + minuto + " : " + segundo

            document.form_reloj.reloj.value = horaImprimible

            setTimeout("tiempo()", 1000)
        }
    </script>

  <style>
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #2C3E50;
    font-size:16px;
    font-family: "Lato", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}

li {
    float: left;
 
}

li:last-child {
    border-right: none;
}

li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

li a:hover:not(.active) {
       color: #18BC9C;
}

.active {
    color: #18BC9C;
}
</style>

<body onload="tiempo()">
    
            <b>
               
            </b>

                <div class="collapse navbar-collapse" id="navbarColor01">

                    <ul class="navbar-nav mr-auto">
                        <li>
                        <a style="font-size:20px;" href="http://localhost:65294/administrador/AdEMP/">AdEMP</a>
                        </li>
                        <li  class="nav-item active">
                            <a class="active"href="http://localhost:65294/administrador/">Inicio </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://localhost:65294/administrador/empleados/">Empleados </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://localhost:65294/administrador/Modificar/">Modificaciones </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://localhost:65294/administrador/Estadisticas/">Revisar estadisticas </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://localhost:65294/administrador/Contacto/">Contacto </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://localhost:65294/administrador/Desconectar/">Cerrar sesion </a>
                        </li>
                    </ul>
                </div>

    <div id="piechart" style="width: 900px; height: 500px; padding-left:320px; position:relative;"></div>

    <a style="text-align:center;text-decoration:none; padding-left:600px; position:relative; font-size:24px;" href="http://localhost:65294/administrador/">Volver al Inicio</a>


       <footer>
        <form name="form_reloj" style="text-align:right">
            <input name="reloj" size="10" style="text-align:center;" />
        </form>
    </footer>
   
</body>
