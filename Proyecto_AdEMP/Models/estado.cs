﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Proyecto_AdEMP.Models
{
    public class estado
    {

        public int codigo { get; set; }

        public string Estado { get; set; }

        public List<estado> Estadorecogido { get; set; }
    }
}