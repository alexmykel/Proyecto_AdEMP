﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Proyecto_AdEMP;
using Proyecto_AdEMP.Models;
using Proyecto_AdEMP.Controllers;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.IO;

namespace Proyecto_AdEMP.Controllers
{
    public class EmpleadosController : Controller
    {
        private AdEMPEntities db = new AdEMPEntities();

        //Inicio Sesion Usuario Empleado
        public ActionResult Index()
        {
            if (Session["DNI"] != null)
            {
                ViewBag.estado = new SelectList(db.estado, "codigo", "estado1");
                ViewBag.activo = new SelectList(db.activo, "codigo", "activo1");

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(crearemp usuario)
        {

            if (ModelState.IsValid)
            {
                using (var db = new AdEMPEntities())
                {
                    ViewBag.estado = new SelectList(db.empleado, "estado");
                    db.SaveChanges();
                    ViewBag.estado = new SelectList(db.estado, "codigo", "estado1");
                    return RedirectToAction("Index");
                }
            }

            return View();
        }



        // VISTA compañeros de trabajo
        public ActionResult compañeros(String nombre)
        {
            var empleado = db.empleado.Include(e => e.activo1).Include(e => e.categoria1).Include(e => e.oficio1).Include(e => e.estado1).Include(e => e.horarios);
        
         //Buscador de usuario por nombre de compañeros
            var nombrebusqueda = from usuario in db.empleado select usuario;
            if (!String.IsNullOrEmpty(nombre))
            {
                nombrebusqueda = nombrebusqueda.Where(model => model.nombre.Contains(nombre));
            }
            return View(nombrebusqueda);
        }


        // Vista horario trabajador
        public ActionResult horario(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            empleado empleado = db.empleado.Find(id);
            if (empleado == null)
            {
                return HttpNotFound();
            }
            return View(empleado);
        }


        //Informacion AdEMP
        public ActionResult AdEMP()
        {
            return View();
        }

        //Informacion contacto AdEMP
        public ActionResult Contacto()
        {
            return View();
        }

        //Accion de desconectar
        public ActionResult Desconectar(string id)
        {
            empleado empleado = db.empleado.Find(id);
            return View(empleado);            
        }

        [HttpPost]
        public ActionResult Desconectar(empleado objUser)
        {
            if (ModelState.IsValid)
            {
                using (AdEMPEntities db = new AdEMPEntities())
                {
                    //Deberemos indicar el usuario para cerrar sesion
                    var obj = db.empleado.Where(session => session.usuario.Equals(objUser.usuario)).FirstOrDefault();
                    try
                    {
                        if (objUser.usuario == obj.usuario.ToString())
                        {
                            //Al coincidir el usuario registrara el dato obtenido de la hora en la que se ha desconectado 
                                                                    //y cambio de activo a 2(false)
                            obj.ult_conex = DateTime.Now;
                            obj.activo = 2;

                            db.SaveChanges();
                            Session.Abandon();
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            //SI EL VALOR ES NULO NOS DEVUELVE A LA VISTA DE DESCONECTAR
                            return RedirectToAction("Desconectar", "Empleados");
                        }
                    }
                    catch
                    {
                        return RedirectToAction("Desconectar", "Empleados");
                    }
                }
            }
            return View(objUser);



        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
